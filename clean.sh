#!/bin/bash

set -e

cd "$(dirname "$0")" # Make sure we are where this script is, in order to not mess up other directories.

if [ -d build ]; then
  rm -rf build
fi
