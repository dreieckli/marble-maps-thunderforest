#!/bin/bash

set -e

THUNDERFOREST_QUERY_PLACEHOLDER='%%THUNDERFOREST_QUERY%%'

printf '%s\n' "Please enter your API key for thunderforest.com."
printf '%s\n' "Leave empty if you have none, but this might result in watermarked or non-functioning tiles."
printf '%s\n' "You can register for free to obtain your own API key at https://www.thunderforest.com/."
read -e -p 'API key: ' THUNDERFOREST_APIKEY

if [ -n "${THUNDERFOREST_APIKEY}" ]; then
  THUNDERFOREST_QUERY="apikey=${THUNDERFOREST_APIKEY}"
else
  THUNDERFOREST_QUERY=''
fi

patch_for_thunderforest_query() {
  # Replaces in place each occurance of "$THUNDERFOREST_QUERY_PLACEHOLDER" with "$THUNDERFOREST_QUERY" in the specified file.
  # Arguments:
  # - $1: Filename to patch.

  sed -i "s|${THUNDERFOREST_QUERY_PLACEHOLDER}|${THUNDERFOREST_QUERY}|g" "$1"
}

cd "$(dirname "$0")" # Make sure we are where this script is, in order to not mess up other directories.

printf '%s\n' "Cleaning up old build ..."
./clean.sh

for _mapsrc in "src"/thunderforest-*; do
  _mapname="$(basename "${_mapsrc}")"
  printf '%s\n' "${_mapname}"
  cp -a "${_mapsrc}" build/
  { 
    for _dgml in "build/${_mapname}"/*.dgml; do
      printf '%s\n' "Patching API-key into '${_dgml}'"
      patch_for_thunderforest_query "${_dgml}"
    done
  } | while read _line; do printf '%s\n' "  ${_line}"; done
done
