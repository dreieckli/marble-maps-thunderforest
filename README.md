# marble-maps-thunderforest

This provides map definitions for "[KDE Marble](https://apps.kde.org/marble/)" for the [maps provided by thunderforest.com](https://www.thunderforest.com/maps/).

Since Thunderforest needs an API key, the map definitions are skeletons which have a placeholder that needs to be replaced by `apikey=<your thunderforest API key>`. If you do not have an API key, you can also _completely_ remove the placeholders, this will result in watermarked map tiles.

A [build script](./build.sh) is included which asks for the API key and automates that patching; the finished map definitions will then be in a subdirectory `build/`.
The [cleanup script](./cleanup.sh) removes the directory `build/`.

Map tiles are provided and copyrighted by [Thunderforest](http://www.thunderforest.com/), data is provided and copyrighted by [OpenStreetMap contributors](http://www.openstreetmap.org/copyright). See [Thunderforest's licensing](https://www.thunderforest.com/terms/).

For licensing see [`COPYING.txt`](./COPYING.txt).
